module ApiHelper
  def json
    Oj.load(response.body)
  end
end
