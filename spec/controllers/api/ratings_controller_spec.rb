describe Api::RatingsController do
  subject { create(:post) }

  describe '#create' do
    let(:post_data) { { rating: { value: 5, post_id: subject.id } } }

    context 'when post with good params' do
      it 'successfully create' do
        post :create, params: post_data, format: :json

        expect(response.status).to eq(201)
        expect(json[:rating_avg].to_i).to eq(5)
      end
    end

    context 'when post ratings for one post few times' do
      before do
        create(:rating, value: 2, post: subject)
      end

      it 'successfully count average rating' do
        post :create, params: post_data, format: :json

        expect(response.status).to eq(201)
        expect(json[:rating_avg].to_f).to eq(3.5)
      end
    end

    context 'when post rating with negative value' do
      it 'response with unprocessable_entity' do
        post_data[:rating][:value] = -1
        post :create, params: post_data, format: :json

        expect(response.status).to eq(422)
        expect(json[:value]).to_not be_nil
      end
    end

    context 'when post rating to nonexistent post' do
      it 'response with unprocessable_entity' do
        post_data[:rating][:post_id] = -1
        post :create, params: post_data, format: :json

        expect(response.status).to eq(422)
        expect(json[:post]).to_not be_nil
      end
    end
  end
end
