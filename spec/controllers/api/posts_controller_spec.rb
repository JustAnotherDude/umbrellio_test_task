describe Api::PostsController do
  describe '#create' do
    let(:username) { 'testuser' }
    let(:post_data) do
      {
        post: {
          title: 'Title',
          body: 'Body',
          author_ip: '127.0.0.1',
          username: username
        }
      }
    end

    context 'when post with good params' do
      it 'successfully create' do
        post :create, params: post_data, format: :json

        expect(response.status).to eq(201)
      end
    end

    context 'when post with bad title' do
      it 'response with unprocessable_entity' do
        post_data[:post].delete(:title)
        post :create, params: post_data, format: :json

        expect(response.status).to eq(422)
        expect(User.find_by(username: username)).to be_nil
      end
    end

    context 'when post with bad username' do
      it 'response with unprocessable_entity' do
        post_data[:post].delete(:username)
        post :create, params: post_data, format: :json

        expect(response.status).to eq(422)
        expect(json).to include(:username)
      end
    end

    context 'when post with existed author' do
      subject { create(:user) }

      it 'link new post with existed user' do
        post_data[:post][:username] = subject.username
        post :create, params: post_data, format: :json

        expect(response.status).to eq(201)
        expect(subject.posts.count).to eq(1)
      end
    end
  end

  describe '#top' do
    subject { create(:post, :with_raiting) }
    let(:fill_with_posts) { create_list(:post, 5) }

    before do
      subject
      fill_with_posts
    end

    context 'when get top of five posts' do
      it 'response with array of 5 posts' do
        get :top, params: { number_of_posts: 5 }, format: :json

        expect(response.status).to eq(200)
        expect(json.count).to eq(5)
        expect(json.first[:title]).to eq(subject.title)
      end
    end

    context 'when get top of -1 posts' do
      it 'response with bad_request' do
        get :top, params: { number_of_posts: -1 }, format: :json

        expect(response.status).to eq(400)
      end
    end
  end
end
