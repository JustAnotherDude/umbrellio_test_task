describe Api::UsersController do
  subject { create(:user, :with_ips) }

  describe '#index' do
    let(:ip) { subject.ips.first }
    let(:top_ips_hash) { { ip.value => ip.users.map { |user| user.username } } }

    before do
      another_user = create(:user)
      another_user.ips << ip
    end

    context 'when get list of ips with few users' do
      it 'response with ips' do
        get :index, format: :json

        expect(response.status).to eq(200)
        expect(json.count).to eq(1)
      end
    end
  end
end
