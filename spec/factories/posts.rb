FactoryBot.define do
  factory :post do
    title { FFaker::Book.title }
    body { FFaker::Book.description }
    author_ip { FFaker::Internet.ip_v4_address }
    user { create(:user) }

    after(:create) do |post|
      form = IpForm.new(value: post.author_ip)
      form.save

      post.user.ips << form.model unless post.user.ips.include? form.model
    end

    trait :with_raiting do
      transient do
        count { 50 }
      end

      after(:create) do |post, evaulator|
        create_list(:rating, evaulator.count, post: post)
      end
    end
  end
end
