FactoryBot.define do
  factory :user do
    username { FFaker::Internet.user_name }

    trait :with_ips do
      transient do
        count { 20 }
      end

      after(:create) do |user, evaulator|
        create_list(:ip, evaulator.count, users: [user])
      end
    end
  end
end
