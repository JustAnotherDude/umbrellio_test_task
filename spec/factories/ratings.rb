FactoryBot.define do
  factory :rating do
    value { FFaker::Random.rand(0..5) }
    post { create(:post) }
  end
end
