class User < ApplicationRecord
  has_many :posts, dependent: :destroy
  has_and_belongs_to_many :ips

  def self.ip_with_many_users
    joins(:posts)
      .select('posts.author_ip ip, ARRAY_AGG(DISTINCT users.username) usernames')
      .group('posts.author_ip')
      .having('COUNT(DISTINCT users.username) > 1')
  end
end
