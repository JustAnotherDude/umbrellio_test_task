module Api
  class RatingsController < ApplicationController
    def create
      form = RatingForm.new(rating_params)
      if form.save
        render json: { rating_avg: Rating.average_rating_for_post(rating_params[:post_id]) }, status: :created
      else
        render json: form.errors, status: :unprocessable_entity
      end
    end

    private

    def rating_params
      params.require(:rating).permit(:value, :post_id)
    end
  end
end
