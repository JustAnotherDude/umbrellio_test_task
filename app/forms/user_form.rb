class UserForm < ApplicationForm
  define_model :user

  attribute :username, String

  validates :username, presence: true, unless: :persisted?
  validates :username, length: { minimum: 1 }, allow_nil: true

  def username=(value)
    @username = value&.downcase
    @model = model.class.find_by(username: @username) || User.new unless persisted?
  end
end
