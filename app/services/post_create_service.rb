class PostCreateService < ApplicationService
  def initialize(post_params)
    @username = post_params[:username]
    @post_params = post_params.except(:username).symbolize_keys
  end

  def call
    post_form = nil

    ActiveRecord::Base.transaction do
      user_form = UserForm.new(username: @username)
      user_form.save unless user_form.persisted?
      return user_form if user_form.errors.present?

      post_form = PostForm.new(**@post_params, user_id: user_form.model.id)
      raise ActiveRecord::Rollback unless post_form.save

      ip_form = IpForm.new(value: @post_params[:author_ip])
      ip_form.save
      ip_form.model.users << user_form.model unless ip_form.model.users.include? user_form.model
    rescue ActiveRecord::RecordNotUnique
    end

    post_form
  end
end
