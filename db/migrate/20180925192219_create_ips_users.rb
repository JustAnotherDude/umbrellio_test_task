class CreateIpsUsers < ActiveRecord::Migration[5.2]
  def change
    create_table :ips_users do |t|
      t.belongs_to :user, index: true
      t.belongs_to :ip, index: true
    end

    add_index :ips_users, %i[user_id ip_id], unique: true
  end
end
