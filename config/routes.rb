Rails.application.routes.draw do
  namespace :api, defaults: { format: :json } do
    resource :posts, only: :create do
      get :top
    end
    get '/users', to: 'users#index'
    resource :ratings, only: :create
  end
end
